#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <time.h>
#include <omp.h>

void write_ll_array(long long a[], int size){
	FILE *out=fopen("out.txt","w");
	int i;
	for(i=0;i<size;i++)
		fprintf(out, "%llx\n", a[i]);
	fclose(out);
}

void print_int_array(int a[], int size){
	int i;
	for(i=0;i<size-1;i++)
		printf("%d, ", a[i]);
	printf("%d\n", a[size-1]);
}

void partition(long long a[], int size, int pivot){
	//fprintf(stderr, "size=%d, pivot=%d\n", size, pivot);
	if(size<=1) return;
	long long temp;
	if(size==2){
		if(a[0]>a[1]){
			temp=a[0];
			a[0]=a[1];
			a[1]=temp;
		}
		return;
	}
	int i, g_size=0, l_size=0;
	long long greater[size], less[size];
	temp=a[pivot];
	for(i=0; i<pivot; i++)
		if(a[i]<temp)
			less[l_size++]=a[i];
		else
			greater[g_size++]=a[i];
	//skip pivot	
	for(i=pivot+1; i<size; i++)
		if(a[i]<temp)
			less[l_size++]=a[i];
		else
			greater[g_size++]=a[i];
	
	partition(less, l_size, l_size/2);
	partition(greater, g_size, g_size/2);
	
	memcpy(a, less, (l_size)*sizeof(long long));
	a[l_size]=temp;
	memcpy(&a[l_size+1], greater, (g_size)*sizeof(long long));	
}


void sort(long long a[], int size){
	/* Swaped and finished_mask are guards for the for loops.
	 * The partitions array holds the index of the highest
	 * element with that mask. */
	int i, j, swaped=1, finished_mask=1, partitions[16], p_index=14;
	partitions[15]=size-1;
	unsigned long long mask, temp;
	//Use mask to split the array between the larger and smaller elements.
	for(mask=0xf000000000000000;mask;mask>>=4){
		for(i=partitions[p_index+1]; finished_mask&&i>=0;i--){
			//If suitable number found swaped set to true so for returns.
			for(j=i;swaped&&j>=0;j--){
				if(a[j]&mask){
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
					swaped=0;
				}else if(j==0)
					finished_mask=0;
			}
			swaped=1;
		}
		if(p_index>-1) partitions[p_index--]=i+1;
		finished_mask=1;
	}
	fprintf(stderr, "a=%d\n", partitions[0]);
	print_int_array(partitions, 16);
	int length, start;
	#pragma omp parallel
	{
		#pragma omp single
		{
			for(i=0;!partitions[i]&&i<16;i++);
			length=partitions[i]-partitions[i-1]+1;
			#pragma omp task firstprivate(length)
			{
				partition(a,length,length/2);
			}
			for(;i<16;i++){
				if(length=partitions[i]-partitions[i-1]){
					#pragma omp task firstprivate(start, length, i)
					{
						start=partitions[i-1]+1;
						fprintf(stderr, "start=%d\n", start);
						partition(&a[start],length,length/1);
						fprintf(stderr, "done partition %d\n", i);
					}
				}
			}
		}
	}
}
#define LENGTH 30000
int main(){
	srand(time(NULL));
	long long array[LENGTH];
	int i;
	for(i=0;i<LENGTH;i++){
		array[i]=rand();
		array[i]<<=32;
		array[i]=array[i]|(long long)rand();
	}
	//partition(array, LENGTH, LENGTH/2);
	sort(array, LENGTH);

	write_ll_array(array, LENGTH);
}
